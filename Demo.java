package ship;

import java.util.Scanner;

public class Demo {
    private static Scanner reader = new Scanner(System.in);



   public static void main(String[] args) {

     Ship ship = new Ship((byte) (Math.random() *9)); 
     System.out.println("Приветствую вас в игре под название Кораблик!(Над названием ещё идёт работа)." +
                "\nСуть игры в том, что дана лента состоящая из ячеек и нужно делать выстрелы, чтобы попасть по кораблю." +
                "\nКорабль состоит из 3-х ячеек." +
                "\nЧтож, начнём игру!" +
                "\nВведите координату выстрела от 0 до 11.");

       byte coordinate;
       int count = 0;/*кол-во попыток*/
        
       while(!ship.floded()) {
           coordinate = reader.nextByte();
           while(coordinate < 0 || coordinate > 11) {
                System.out.println("Вы ошиблись, введите корректное значение. ");
                coordinate = reader.nextByte();
            }
            ship.test(coordinate);
           
            count++;
        }
        System.out.println("Поздарвляю! Вы потопили корабль за " + count + " попыток.");
    }
}