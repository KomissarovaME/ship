package ship;

import java.util.ArrayList;

public class Ship {

    private static final String FLOODED_SHIP = "Вы потопили корабль, поздравляю!";
    private static final String HIT_INTO_SHIP = "Попали, продолжайте в том же духе!";
    private static final String BLUNDER = "Вы промахнулись, попробуйте ещё раз.";

    private ArrayList<Byte> location = new ArrayList<Byte>();
/*
*Конструктор для координат коробля.
*/
    public Ship(byte location){
        this.location.add(location);
        this.location.add((byte) (location + 1));
        this.location.add((byte) (location + 2));
    }
/*
*Метод, который получает кооринату и проверяет её наличие у этого кораБЛЯ,
 * и удаляет из ArrayList'а.
*/
    public void test(byte coordinate){
        if(location.contains(coordinate)){
            location.remove(location.indexOf(coordinate));
            System.out.println(HIT_INTO_SHIP);
        }else{ System.out.println(BLUNDER);}
    }
/*
* При пустом ArrayList'е выводится сообщение FLOODED_SHIP.
*/
    public boolean floded(){
        if(location.isEmpty()){
            System.out.println(FLOODED_SHIP);
            return true;
        }else{
            return false;
        }
    }
}
